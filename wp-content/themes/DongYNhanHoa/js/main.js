 ;(function($) {
    $(document).ready(function(){
		$(".content-message").addClass("hidden");
		$(".message-list a").on("click", function(e) {
	        var element = $(this).parent().find(".content-message");
			var hidden = element.hasClass('hidden');
			$(".content-message").addClass("hidden");
			if(hidden) {
				element.removeClass('hidden');
			}
		})
		$('.bxslider').bxSlider({
		  	responsive: false,
		  	controls: false,
		  	auto: true,
		  	speed: 250
		});
	});
	
})(jQuery);