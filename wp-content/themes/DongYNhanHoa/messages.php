<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Kogumakai
 * @since Kogumakai 1.0
 *
 * The template for displaying all pages
 * Template Name: Message Page
 */

get_header(); ?>

	<main>
		<section class="products messages">
			<div class="container">
				<div class="row">
					<div class="conl-md-12 message-list">
						<div class="products-heading">
							<h3 class="text">Thông báo của chúng tôi</h3>
						</div>
						<ul>
							<li>
								<a href="javascript:void(0)" title="">Lịch nghỉ tết nguyên đán 2016</a>
		               			<div class="content-message">
		               				<h4><i class="fa fa-arrow-right" aria-hidden="true"></i>Lịch nghỉ tết nguyên đán 2016</h4>
		               				<p>
		               					Kogumakai là chương trình phát triển TƯ DUY LOGIC theo phương pháp Nhật Bản dành cho bé từ 3 – 6 tuổi.
		               				</p>
		               				<p>
										Để phát triển tư duy logic một cách toàn diện bé không những cần được trang bị khả năng tư duy về toán học mà còn phải có được khả năng suy luận, so sánh, phán đoán và suy lý.
									</p>
									<p>
										Kogumakai vận dụng kiến thức nền tảng Toán học để cung cấp cho bé những kiến thức cơ bản, đồng thời tạo cho bé cơ hội phát triển một cách toàn diện nhất về mặt tư duy logic (suy lý – giải thích – xử lý tình huống/ vấn đề phát sinh), thúc đẩy khả năng sáng tạo thông qua các trò chơi, trải nghiệm với vật thật; rèn luyện và hoàn thiện hệ ngôn ngữ ở trẻ.
									</p>
									<p>
										Do đó, Kogumakai không đơn thuần giảng dạy về mặt toán học.
		               				</p>	
		               			</div>
							</li>
							<li>
								<a href="javascript:void(0)" title=""> Lịch nghỉ nhân ngày 20/4 - 1/5 (2017)</a>
		               			<div class="content-message">
		               				<h4><i class="fa fa-arrow-right" aria-hidden="true"></i>Lịch nghỉ nhân ngày 20/4 - 1/5 (2017)</h4>
		               				<p>
		               					Kogumakai là chương trình phát triển TƯ DUY LOGIC theo phương pháp Nhật Bản dành cho bé từ 3 – 6 tuổi.
		               				</p>
		               				<p>
										Để phát triển tư duy logic một cách toàn diện bé không những cần được trang bị khả năng tư duy về toán học mà còn phải có được khả năng suy luận, so sánh, phán đoán và suy lý.
									</p>
									<p>
										Kogumakai vận dụng kiến thức nền tảng Toán học để cung cấp cho bé những kiến thức cơ bản, đồng thời tạo cho bé cơ hội phát triển một cách toàn diện nhất về mặt tư duy logic (suy lý – giải thích – xử lý tình huống/ vấn đề phát sinh), thúc đẩy khả năng sáng tạo thông qua các trò chơi, trải nghiệm với vật thật; rèn luyện và hoàn thiện hệ ngôn ngữ ở trẻ.
									</p>
									<p>
										Do đó, Kogumakai không đơn thuần giảng dạy về mặt toán học.
		               				</p>	
		               			</div>
							</li>
							<li>
								<a href="javascript:void(0)" title=""> Lịch nghỉ nhân ngày quốc khánh (2017)</a>
		               			<div class="content-message">
		               				<h4><i class="fa fa-arrow-right" aria-hidden="true"></i>Lịch nghỉ nhân ngày quốc khánh (2017)</h4>
		               				<p>
		               					Kogumakai là chương trình phát triển TƯ DUY LOGIC theo phương pháp Nhật Bản dành cho bé từ 3 – 6 tuổi.
		               				</p>
		               				<p>
										Để phát triển tư duy logic một cách toàn diện bé không những cần được trang bị khả năng tư duy về toán học mà còn phải có được khả năng suy luận, so sánh, phán đoán và suy lý.
									</p>
									<p>
										Kogumakai vận dụng kiến thức nền tảng Toán học để cung cấp cho bé những kiến thức cơ bản, đồng thời tạo cho bé cơ hội phát triển một cách toàn diện nhất về mặt tư duy logic (suy lý – giải thích – xử lý tình huống/ vấn đề phát sinh), thúc đẩy khả năng sáng tạo thông qua các trò chơi, trải nghiệm với vật thật; rèn luyện và hoàn thiện hệ ngôn ngữ ở trẻ.
									</p>
									<p>
										Do đó, Kogumakai không đơn thuần giảng dạy về mặt toán học.
		               				</p>	
		               			</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
	</main>


<?php
get_footer();
?>