<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Dong Y
 * @since Dong Y
 *
 * The template for displaying all pages
 * Template Name: Homepage
 */

get_header(); ?>

	<main>
		<section class="slide">
			<!-- <img src="<?php echo THEME_URL; ?>img/slide.png"> -->
			<?php if( have_rows('sliders') ): ?>
				<ul class="bxslider">
				<?php while( have_rows('sliders') ): the_row(); 
					$image = get_sub_field('hinh_anh');?>
					<li><img src="<?php echo $image; ?>"/></li>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</section><!-- slide -->
		<section class="feeling">
			<div class="container">
				<div class="row">
					<div class="conl-md-12">
						<div class="feeling-heading">
							<h3 class="text">cảm nhận của bệnh nhân</h3>
						</div><!-- feeling-heading -->
						<div class="col-md-4 sidebar">
							<ul>
								<li class="danh-muc">danh mục</li>
								<li><a href="#">Đặt mua thuốc</a><i class="icon"></i></li>
								<li><a href="">Dịch vụ</a><i class="icon"></i></li>
								<li><a href="">Trung tâm trị liệu</a><i class="icon"></i></li>
								<li><a href="">Khám chữa bệnh tại nhà</a><i class="icon"></i></li>
								<li><a href="">Đặt lịch khám</a><i class="icon"></i></li>
								<li><a href="">Bệnh đã chữa khỏi</a><i class="icon"></i></li>
								<li><a href="">Bệnh đặc trị</a><i class="icon"></i></li>
								<li><a href="">Cảm tưởng của bệnh nhân</a><i class="icon"></i></li>
							</ul>
						</div><!-- sidebar -->
						<div class="col-md-8 video">
							<?php if( have_rows('videos') ): ?>
							<div class="youtube">
								<?php while( have_rows('videos') ): the_row(); 
									$video = get_sub_field('video');?>
										<div class="col-md-6"><iframe width="370" height="275" src="<?php echo $video; ?>" frameborder="0" allowfullscreen></iframe></div>
								<?php endwhile; ?>
							</div>
							<?php endif; ?>
							<div class="hotline">hotline : <?php echo get_field('hotline', 'options'); ?></div>
						</div><!-- video -->
					</div>
				</div>
			</div>
		</section><!-- feeling -->
		<section class="booking">
			<div class="booking-online">
				<div class="container">
					<div class="row">
						<div class="col-md-6 text">Hỗ trợ tư vấn trực tuyến</div>
						<div class="col-md-6 text">đặt lịch ngay</div>
					</div>
				</div>
			</div><!-- booking-online -->
			<div class="slide">
				<img src="<?php echo THEME_URL; ?>img/slide.png">
			</div><!-- slide -->
		</section><!-- booking -->
		<section class="products">
			<div class="container">
				<div class="row">
					<div class="conl-md-12">
						<div class="products-heading">
							<h3 class="text">sản phẩm của chúng tôi</h3>
						</div><!-- products-heading -->
						<div class="col-md-4 sidebar">
							<ul>
								<li class="danh-muc">Sản phẩm</li>
								<li><a href="#">Đặt mua thuốc</a><i class="icon"></i></li>
								<li><a href="">Dịch vụ</a><i class="icon"></i></li>
								<li><a href="">Trung tâm trị liệu</a><i class="icon"></i></li>
								<li><a href="">Khám chữa bệnh tại nhà</a><i class="icon"></i></li>
								<li><a href="">Đặt lịch khám</a><i class="icon"></i></li>
								<li><a href="">Bệnh đã chữa khỏi</a><i class="icon"></i></li>
								<li><a href="">Bệnh đặc trị</a><i class="icon"></i></li>
								<li><a href="">Cảm tưởng của bệnh nhân</a><i class="icon"></i></li>
							</ul>
						</div><!-- sidebar -->
						<div class="col-md-8 product-list">
							<div class="product-item">
								<div class="col-md-6">
									<div class="avatar"><img src="<?php echo THEME_URL; ?>img/soithan.png"></div>
									<div class="title"><a href="#">So gan</a></div>
								</div>
								<div class="col-md-6">
									<div class="avatar"><img src="<?php echo THEME_URL; ?>img/soithan.png"></div>
									<div class="title"><a href="#">So gan</a></div>
								</div>
								<div class="col-md-6">
									<div class="avatar"><img src="<?php echo THEME_URL; ?>img/soithan.png"></div>
									<div class="title"><a href="#">So gan</a></div>
								</div>
								<div class="col-md-6">
									<div class="avatar"><img src="<?php echo THEME_URL; ?>img/soithan.png"></div>
									<div class="title"><a href="#">So gan</a></div>
								</div>
							</div>
						</div><!-- product-list -->
					</div>
				</div>
			</div><!-- container -->
		</section><!-- products -->
		<section class="news">
			<div class="container">
				<div class="row">
					<div class="conl-md-12">
						<div class="feeling-heading">
							<h3 class="text">tin tức - sự kiện</h3>
						</div>
						<div class="news-first">
							<div class="col-md-5">
								<img src="<?php echo THEME_URL; ?>img/tintuc.png">
							</div>
							<div class="col-md-7">
								<h3>Đại hội đaị biểu toàn quốc hội đông y việt Nam  khóa XIII nhiệm</h3>
								<div class="description">
									Đại hội đã đón nhận các Lẵng hoa chúc mừng của Tổng Bí thư Trung ương Đảng Nguyễn Phú Trọng; Chủ tịch Quốc hội Nguyễn Sinh Hùng và Chủ tịch Ủy ban Trung ương Mặt trận tổ quốc Việt Nam Nguyễn Thiện Nhân.
								</div>
							</div>
						</div>
						<div class="news-child">
							<div class="row">
								<div class="col-md-7">
									<ul>
										<li><i class="fa fa-caret-right" aria-hidden="true"></i>Báo Sức khỏe & Đời sống: Đại hội quốc tế lần thứ 3  về Liệu pháp chữa trị bệnh tự nhiên 2016</li>
										<li><i class="fa fa-caret-right" aria-hidden="true"></i>Báo Sức khỏe & Đời sống: Đại hội quốc tế lần thứ 3  về Liệu pháp chữa trị bệnh tự nhiên 2016</li>
										<li><i class="fa fa-caret-right" aria-hidden="true"></i>Báo Sức khỏe & Đời sống: Đại hội quốc tế lần thứ 3  về Liệu pháp chữa trị bệnh tự nhiên 2016</li>
										<li><i class="fa fa-caret-right" aria-hidden="true"></i>Báo Sức khỏe & Đời sống: Đại hội quốc tế lần thứ 3  về Liệu pháp chữa trị bệnh tự nhiên 2016</li>
									</ul>
								</div>
								<div class="col-md-5">
									<h3>gương mặt tiêu biểu</h3>
									<div class="avatar">
										<img src="<?php echo THEME_URL; ?>img/bs.png">
										<h4 class="name">Nguyen Van A</h4>
										<h4 class="chucvu">Bác sĩ đông y</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- news -->
		<section class="images">
			<div class="container">
				<div class="row">
					<div class="images-heading">
						<h3 class="text">hình ảnh</h3>
					</div>
					<div class="images-content">
						<div class="col-md-4">
							<div class="content">
								<div class="img"><img src="<?php echo THEME_URL; ?>img/tintuc1.png"></div>
								<div class="short-des"><span>Đại hội quốc tế lần thứ 3  về Liệu pháp chữa bệnh </span></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="content">
								<div class="img"><img src="<?php echo THEME_URL; ?>img/tintuc2.png"></div>
								<div class="short-des"><span>Đại hội quốc tế lần thứ 3  về Liệu pháp chữa bệnh </span></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="content">
								<div class="img"><img src="<?php echo THEME_URL; ?>img/tintuc3.png"></div>
								<div class="short-des"><span>Đại hội quốc tế lần thứ 3  về Liệu pháp chữa bệnh </span></div>
							</div>
						</div>
					</div><!-- images-content -->
				</div>
			</div>
		</section><!-- images -->
		<section class="maps">
			<div id="map" style="height: 450px; position: relative; overflow: hidden;">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.5251796281664!2d106.67181891435033!3d10.771029992325252!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752edfaf804189%3A0x1ddba67a3c170ead!2zMTA4IFRy4bqnbiBNaW5oIFF1eeG7gW4sIHBoxrDhu51uZyAxMSwgUXXhuq1uIDEwLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1506868128120" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</section>
	</main>

<?php get_footer(); ?>
