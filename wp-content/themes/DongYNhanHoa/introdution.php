<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage DongYNhanHoa
 * @since DongYNhanHoa
 *
 * The template for displaying all pages
 * Template Name: Introdution Page
 */

get_header(); ?>

	<main>
		<section class="introdution-company">
			<div class="container">
				<div class="row">
					<div class="conl-md-12">
						<div class="products-heading">
							<h3 class="text">Về chúng tôi</h3>
							<div class="basic">
								
							</div>
						</div><!-- products-heading -->
					</div>
				</div>
			</div><!-- container -->
		</section><!-- introdution-company -->
	</main>


<?php
get_footer();
?>