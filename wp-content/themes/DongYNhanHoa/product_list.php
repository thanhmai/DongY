<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Kogumakai
 * @since Kogumakai 1.0
 *
 * The template for displaying all pages
 * Template Name: Product List Page
 */

get_header(); ?>

<main>
		<section class="products">
			<div class="container">
				<div class="row">
					<div class="conl-md-12">
						<div class="products-heading">
							<h3 class="text">sản phẩm của chúng tôi</h3>
						</div>
						<div class="col-md-4 sidebar">
							<ul>
								<li class="danh-muc">danh mục</li>
								<li><a href="#">Đặt mua thuốc</a><i class="icon"></i></li>
								<li><a href="">Dịch vụ</a><i class="icon"></i></li>
								<li><a href="">Trung tâm trị liệu</a><i class="icon"></i></li>
								<li><a href="">Khám chữa bệnh tại nhà</a><i class="icon"></i></li>
								<li><a href="">Đặt lịch khám</a><i class="icon"></i></li>
								<li><a href="">Bệnh đã chữa khỏi</a><i class="icon"></i></li>
								<li><a href="">Bệnh đặc trị</a><i class="icon"></i></li>
								<li><a href="">Cảm tưởng của bệnh nhân</a><i class="icon"></i></li>
							</ul>
						</div>
						<div class="col-md-8 product-list">
							<div class="product-item">
								<div class="col-md-6">
									<div class="avatar"><img src="<?php echo THEME_URL; ?>img/soithan.png"></div>
									<div class="title"><a href="#">So gan</a></div>
								</div>
								<div class="col-md-6">
									<div class="avatar"><img src="<?php echo THEME_URL; ?>img/soithan.png"></div>
									<div class="title"><a href="#">So gan</a></div>
								</div>
								<div class="col-md-6">
									<div class="avatar"><img src="<?php echo THEME_URL; ?>img/soithan.png"></div>
									<div class="title"><a href="#">So gan</a></div>
								</div>
								<div class="col-md-6">
									<div class="avatar"><img src="<?php echo THEME_URL; ?>img/soithan.png"></div>
									<div class="title"><a href="#">So gan</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>

<?php
get_footer();
?>