<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Kogumakai
 * @since Kogumakai 1.0
 *
 * The template for displaying all pages
 * Template Name: Contact Page
 */

get_header(); ?>

	<main>
		<section class="products contact">
			<div class="container">
				<div class="row">
					<div class="conl-md-12">
						<div class="products-heading">
							<h3 class="text">Liên hệ đến chúng tôi</h3>
						</div>
						<div class="infos">
							<div class="item"><span>Địa chỉ:</span><span>C5/8 Phạm Hùng, Phường 4, Quận 8, TPHCM</span></div>
							<div class="item"><span>Hotline: </span><span>0963235372</span></div>
							<div class="item"><span>Mail: </span><span>nhanhoa@gmail.com</span></div>
							<div class="item"><span>Facebook: </span><span>nhanhoa.vn</span></div>
						</div>
						<div id="map" style="height: 450px; position: relative; overflow: hidden;">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.5251796281664!2d106.67181891435033!3d10.771029992325252!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752edfaf804189%3A0x1ddba67a3c170ead!2zMTA4IFRy4bqnbiBNaW5oIFF1eeG7gW4sIHBoxrDhu51uZyAxMSwgUXXhuq1uIDEwLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1506868128120" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>


<?php
get_footer();
?>