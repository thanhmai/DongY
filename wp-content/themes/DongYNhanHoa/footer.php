<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Dong Y Nhan Hoa
 * @since Dong Y Nhan Hoa
 */
?>
	<footer>
		<div class="top-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="logo">
							<img src="<?php echo THEME_URL; ?>img/logo.png">
						</div><!-- logo -->
						<div class="infomation">
							<p>Địa chỉ: <?php echo get_field('dia_chi', 'options'); ?><br>
							Email: <?php echo get_field('mail', 'options'); ?><br>
							Hotline: <?php echo get_field('hotline', 'options'); ?><br>
							Facebook: <?php echo get_field('facebook', 'options'); ?><br>
							Giờ mở cửa: <?php echo get_field('gio_mo_cua', 'options'); ?> - <?php echo get_field('gio_dong_cua', 'options'); ?></p>
						</div><!-- infomation -->
					</div>
					<div class="col-md-4">
						<h3>Cảm nhận của bệnh nhân</h3>
						<div class="person-feeling">
							<div class="quotes">
								<i class="fa fa-quote-left" aria-hidden="true"></i>Đại hội đã đón nhận các Lẵng hoa chúc mừng của Tổng Bí thư Trung ương Đảng Nguyễn Phú Trọng; Chủ tịch Quốc hội Nguyễn Sinh Hùng và Chủ tịch Ủy ban Trung ương Mặt trận tổ quốc Việt Nam Nguyễn Thiện Nhân.
							</div>
							<div class="time">3 day ago</div>
						</div><!-- person-feeling -->
						<div class="person-feeling">
							<div class="quotes">
								<i class="fa fa-quote-left" aria-hidden="true"></i>Đại hội đã đón nhận các Lẵng hoa chúc mừng của Tổng Bí thư Trung ương Đảng Nguyễn Phú Trọng; Chủ tịch Quốc hội Nguyễn Sinh Hùng và Chủ tịch Ủy ban Trung ương Mặt trận tổ quốc Việt Nam Nguyễn Thiện Nhân.
							</div>
							<div class="time">3 day ago</div>
						</div>
					</div><!-- person-feeling -->
					<div class="col-md-4">
						<h3>Đội trưởng</h3>
						<div class="note">
							<span>Receive alerts about our latest updates, promotions and specials to your inbox!</span>
						</div>
						<div class="input-group">
						  	<input type="text" class="form-control" placeholder="Recipient's username" aria-describedby="basic-addon2">
						  	<span class="input-group-addon" id="basic-addon2">@example.com</span>
						</div>
						<div class="footer-socials">
							<ul class="socials">
								<li><i class="fa fa-facebook" aria-hidden="true"></i></li>
								<li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
								<li><i class="fa fa-twitter" aria-hidden="true"></i></li>
								<li><i class="fa fa-youtube-square" aria-hidden="true"></i></li>
							</ul>
						</div><!-- footer-socials -->
					</div>
				</div>
			</div><!-- container -->
		</div><!-- top-footer -->
		<div class="bottom-footer copyright">
			Copyright © nhanhoa.vn. All rights reserved.
		</div><!-- bottom-footer -->
	</footer><!-- footer -->
</body>
</html>
