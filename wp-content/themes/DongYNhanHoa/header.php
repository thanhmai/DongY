<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Dong Y Nhan Hoa
 * @since Dong Y Nhan Hoa
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header>
		<div class="top-header">
			<div class="container">
				<div class="row">
					<div class="top-left">
						<span class="phone-number"><i class="fa fa-phone" aria-hidden="true"></i><?php echo get_field('hotline', 'options'); ?></span>
						<span class="hot-mail"><i class="fa fa-envelope-o" aria-hidden="true"></i><?php echo get_field('mail', 'options'); ?></span>
						<span class="time"><i class="fa fa-clock-o" aria-hidden="true"></i>(T2 - T7): <?php echo get_field('gio_mo_cua', 'options'); ?> - <?php echo get_field('gio_dong_cua', 'options'); ?></span>
					</div><!-- top-left -->
					<div class="top-right">
						<ul class="socials">
							<li><i class="fa fa-facebook" aria-hidden="true"></i></li>
							<li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
							<li><i class="fa fa-twitter" aria-hidden="true"></i></li>
							<li><i class="fa fa-youtube-square" aria-hidden="true"></i></li>
						</ul>
					</div><!-- top-right -->
				</div>
			</div>
		</div><!-- top-header -->
		<div class="menu-header">
			<div class="container">
				<div class="row">
					<div class="logo col-md-5">
						<img src="<?php echo THEME_URL; ?>img/logo.png">
					</div>
					<div class="menu col-md-7">
						<ul class="menus">
							<li class="active"><a href="<?php echo home_url(); ?>">Trang Chủ</a></li>
							<li><a href="<?php echo home_url('introdution.html'); ?>">Giới thiệu</a></li>
							<li><a href="">Lịch làm việc</a></li>
							<li><a href="<?php echo home_url('messages-page'); ?>">Thông báo</a></li>
							<li><a href="<?php echo home_url('contact-page'); ?>">Liên hệ</a></li>
						</ul>
						<div class="search">
							<span class="icon-search"><i class="fa fa-search" aria-hidden="true"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div><!-- menu-header -->
	</header><!-- .site-header -->